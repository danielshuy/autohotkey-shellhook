# AutoHotkey ShellHook changelog

1.0.1
- Included AutoHotkey OnExit Constants library version 1.0.0 (OnExitConst-1.0.0)

1.0.0
- Initial Rev
